package com.techbulltest;

import com.techbulltest.models.MovieResponse;

public interface ResponseCallback {
    void onResponse(MovieResponse movieResponse);
}
