package com.techbulltest;

import android.util.Log;

import com.techbulltest.models.MovieResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiCalls {
    private static final String TAG = "ApiCalls";
    public void getMovies(ResponseCallback responseCallback,String query,String page){
        Log.d(TAG, "getMovies: "+page);
        Call<MovieResponse> call=ApiClient.getRetrofit().getMovies(query,page,"901e9009");
        call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                if(response.code()==200){
                    responseCallback.onResponse(response.body());
                }
            }

            @Override
            public void onFailure(Call<MovieResponse> call, Throwable t) {

            }
        });
    }
}
