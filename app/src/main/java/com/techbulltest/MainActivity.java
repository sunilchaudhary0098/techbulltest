package com.techbulltest;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.techbulltest.models.MovieResponse;
import com.techbulltest.models.SearchItem;

import java.util.LinkedList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ResponseCallback {

    private static final String TAG = "MainActivity";
    ApiCalls apiCalls;
    MoveiAdapter moveiAdapter;
    List<SearchItem>itemList=new LinkedList<>();
    RecyclerView recycler;
    Button search_btn;
    EditText search_et;
    int pageNo=1;
    String search_query="batman";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recycler=findViewById(R.id.recycler);
        search_btn=findViewById(R.id.search_btn);
        search_et=findViewById(R.id.search_et);

        search_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                search_query=search_et.getText().toString();
                if(search_query.isEmpty()){
                    search_et.setError("Enter query");
                    return;
                }
                if(itemList!=null){
                    itemList.clear();
                    moveiAdapter.addList(itemList);
                }


                apiCalls.getMovies(MainActivity.this::onResponse,search_query,String.valueOf(pageNo));
            }
        });

        apiCalls=new ApiCalls();
        moveiAdapter=new MoveiAdapter(MainActivity.this,itemList);
        recycler.setAdapter(moveiAdapter);
        recycler.addOnScrollListener(new HidingScrollListener(new LinearLayoutManager(MainActivity.this)) {
            @Override
            public void onHide() {
            }

            @Override
            public void onLoadMore(int i) {
                Log.d(TAG, "onLoadMore: "+i);
                pageNo = i;
                apiCalls.getMovies(MainActivity.this::onResponse,search_query,String.valueOf(pageNo));

            }

            @Override
            public void onShow() {

            }
        });
        apiCalls.getMovies(this::onResponse,search_query,"1");
    }

    @Override
    public void onResponse(MovieResponse movieResponse) {
        itemList=movieResponse.getSearch();
        moveiAdapter.addList(itemList);
    }
}