package com.techbulltest.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MovieResponse{

	@SerializedName("Response")
	private String response;

	@SerializedName("totalResults")
	private String totalResults;

	@SerializedName("Search")
	private List<SearchItem> search;

	public String getResponse(){
		return response;
	}

	public String getTotalResults(){
		return totalResults;
	}

	public List<SearchItem> getSearch(){
		return search;
	}
}