package com.techbulltest;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.techbulltest.models.SearchItem;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class MoveiAdapter extends RecyclerView.Adapter<MoveiAdapter.ProductViewHolder> {

    private Context mCtx;
    private List<SearchItem> productList;
    SearchItem item;

    public MoveiAdapter(Context mCtx, List<SearchItem> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    public void addList(List<SearchItem>list){
        if(list==null){
         productList.clear();
        }else {
            productList.addAll(list);
        }

        notifyDataSetChanged();
    }
    @NotNull
    @Override
    public MoveiAdapter.ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.movie_item, parent,false);
        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MoveiAdapter.ProductViewHolder holder, final int position) {
        //getting the product of the specified position
        item= productList.get(position);
        holder.name_tv.setText(item.getTitle());
        holder.year_tv.setText(item.getYear());
        Glide.with(holder.imageView).load(item.getPoster()).skipMemoryCache(true).into(holder.imageView);


    }
    @Override
    public int getItemCount() {
        return productList.size();
    }

    static class ProductViewHolder extends RecyclerView.ViewHolder {
        TextView name_tv,year_tv;
        ImageView imageView;
        public ProductViewHolder(View itemView) {
            super(itemView);
            name_tv = itemView.findViewById(R.id.name_tv);
            year_tv = itemView.findViewById(R.id.year_tv);
            imageView = itemView.findViewById(R.id.imageView);
        }
    }


}