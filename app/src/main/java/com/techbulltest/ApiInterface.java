package com.techbulltest;


import com.techbulltest.models.MovieResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("?")
    Call<MovieResponse> getMovies(@Query("s") String s, @Query("page") String page, @Query("apikey") String apikey);


}
