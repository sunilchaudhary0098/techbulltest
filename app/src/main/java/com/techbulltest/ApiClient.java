package com.techbulltest;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiClient {
    private static Retrofit retrofit;
    public static ApiInterface getRetrofit(){
        if(retrofit==null){
            OkHttpClient okHttpClient=new OkHttpClient.Builder().build();
            retrofit=new Retrofit.Builder().baseUrl("https://www.omdbapi.com/")
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit.create(ApiInterface.class);
    }

}
